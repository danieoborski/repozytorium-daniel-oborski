package pl.sda;

public enum Jedzenie {
    TUNCZYK ("Tunczyk"),
    MIESO ("Mieso"),
    PLANKTON ("Plankton"),
    MLEKO ("Mleko"),
    KARMADLAKOTA("Karma dla kota"),
    KARMADLARYB ("Karma dla ryb"),
    MYSZ("Mysz"),
    RESZTKIZOBIADU("Resztki z obiadu");

    private String nazwa;

    Jedzenie(String nazwa) {
        this.nazwa = nazwa;
    }
}
