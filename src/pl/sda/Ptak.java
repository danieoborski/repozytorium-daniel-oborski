package pl.sda;

import java.util.ArrayList;
import java.util.List;

import static pl.sda.Jedzenie.*;

public class Ptak {
    String imie;

    public Ptak(String imie) {
        this.imie = imie;
    }

    public String getImie() {
        return imie;
    }

    public void jedzenieDlaPsa() {
        List<Jedzenie> potrawyPies = new ArrayList<>();
        potrawyPies.add(MIESO);
        potrawyPies.add(MYSZ);
        potrawyPies.add(RESZTKIZOBIADU);
    }

    public void dajGłos() {
        System.out.println("Kra Kra Kra");
    }
    public void przedstawSie(){
        System.out.println(imie);
    }
}
