package pl.sda;

import java.util.ArrayList;
import java.util.List;

import static pl.sda.Jedzenie.*;

public class Kot extends Zwierzak {
    String imie;
    String glos;
    public Kot(String imie) {
        this.imie = imie;
    }

    public String getImie() {
        return imie;
    }

    public String getGłos() {
        return glos;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setGłos(String głos) {
        this.glos = głos;
    }

    public void jedzenieDlaKota() {
        List<Jedzenie> potrawyKot = new ArrayList<>();
        potrawyKot.add(MLEKO);
        potrawyKot.add(KARMADLAKOTA);
        potrawyKot.add(MYSZ);
        potrawyKot.add(MIESO);
        potrawyKot.add(TUNCZYK);
    }
    public void dajGłos(){
        System.out.println("Miauu");
    }
    @Override
    public void przedstawSie(){
        System.out.println(imie);

    }

}
