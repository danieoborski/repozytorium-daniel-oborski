package pl.sda;

import java.util.ArrayList;
import java.util.List;

import static pl.sda.Jedzenie.*;

public class Pies {
    String imie;

    public Pies(String imie) {
        this.imie = imie;
    }

    public String getImie() {
        return imie;
    }

    public void jedzenieDlaPsa(){
        List<Jedzenie> potrawyPies = new ArrayList<>();
        potrawyPies.add(MIESO);
        potrawyPies.add(MLEKO);
        potrawyPies.add(RESZTKIZOBIADU);
    }
    public void dajGłos(){
        System.out.println("Hau Hau Hau");
    }
    public void przedstawSie(){
        System.out.println(imie);
    }
}
