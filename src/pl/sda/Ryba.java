package pl.sda;

import java.util.ArrayList;
import java.util.List;

import static pl.sda.Jedzenie.*;

public class Ryba {
    String imie;

    public Ryba(String imie) {
        this.imie = imie;
    }

    public String getImie() {
        return imie;
    }

    public void jedzenieDlaPsa(){
        List<Jedzenie> potrawyRyba = new ArrayList<>();
        potrawyRyba.add(PLANKTON);
        potrawyRyba.add(KARMADLARYB);
    }
    public void dajGłos() {
        System.out.println("Gul gul gul");
    }
    public void przedstawSie(){
        System.out.println(imie);
    }
}

